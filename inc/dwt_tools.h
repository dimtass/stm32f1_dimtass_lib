#pragma once
#include "stm32f746xx.h"

#ifdef __cplusplus
extern "C" {
#endif

struct tp_dwt_time {
    uint32_t fcpu;
    int s;
    int ms;
    int us;
};

extern uint32_t SystemCoreClock;

void dwt_init()
{
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk | DWT_CTRL_CPIEVTENA_Msk;
}

void dwt_reset(void)
{
    DWT->CYCCNT = 0; /* Clear DWT cycle counter */
}

uint32_t dwt_get_cycles(void)
{
    return DWT->CYCCNT;
}

int dwt_cycles_to_time(uint64_t clks, struct tp_dwt_time *t)
{
    if (!t)
        return -1;
    uint32_t fcpu = SystemCoreClock;
    uint64_t s  = clks / fcpu;
    uint64_t ms = (clks * 1000) / fcpu;
    uint64_t us = (clks * 1000 * 1000) / fcpu;
    ms -= (s * 1000);
    us -= (ms * 1000 + s * 1000000);
    t->fcpu = fcpu;
    t->s = s;
    t->ms = ms;
    t->us = us;
    return 0;
}

float dwt_cycles_to_float_ms(uint64_t clks)
{
    float res;
    float fcpu = (float) SystemCoreClock;
    res = ((float)clks * (float)1000.0) / fcpu;
    return res;
}


/**
 * @brief Blocking non-atomic microseconds delay
 * @param[in] us Number of microseconds
 */
void dwt_delay_us(uint32_t us)
{
    int32_t tp = dwt_get_cycles() + us * (SystemCoreClock/1000000);
    while (((int32_t)DWT_Get() - tp) < 0);
}

/**
 * @brief Blocking non-atomic milliseconds delay
 * @param[in] us Number of milliseconds
 */
void dwt_delay_ms(uint32_t ms)
{
    int32_t tp = dwt_get_cycles() + ms * (SystemCoreClock/1000);
    while (((int32_t)DWT_Get() - tp) < 0);
}


#ifdef __cplusplus
}
#endif