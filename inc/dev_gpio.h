/*
 * dev_gpio.h
 *
 * Copyright 2019 Dimitris Tassopoulos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

#ifndef __DEV_GPIO_H_
#define __DEV_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include "stm32f10x.h"
#include "debug_trace.h"
#include "list.h"

enum en_port_index {
    GPIO_PORT_A = 0,
    GPIO_PORT_B,
    GPIO_PORT_C,
    GPIO_PORT_D,
    GPIO_PORT_E,
    GPIO_PORT_F
};

enum en_gpio_dir {
    GPIO_DIR_INPUT = 0,
    GPIO_DIR_OUTPUT = 1
};

struct tp_gpio {
    uint8_t         port;
    uint16_t        pin;    /* GPIO pin */
    uint8_t         dir;
    uint8_t         init_value;
    uint8_t         invert;
    GPIO_TypeDef *  _port;   /* GPIO port */
    GPIO_InitTypeDef _config;
};

#define DECLARE_GPIO(NAME,PORT,PIN,DIR,INIT,INVERT) \
    struct tp_gpio NAME = { \
        .port = PORT, \
        .pin = PIN, \
        .dir = DIR, \
        .init_value = INIT, \
        .invert = INVERT, \
    }

void gpio_init(struct tp_gpio * gpio);

struct tp_gpio * gpio_init_param(enum en_port_index port, uint16_t pin, enum en_gpio_dir dir, uint8_t init_value, uint8_t invert);

static inline void gpio_set(struct tp_gpio * gpio)
{
    if (gpio->_port == NULL) return;

    if (gpio->dir == GPIO_DIR_INPUT) return;

    if (!gpio->invert)
        gpio->_port->ODR |= gpio->pin;
    else
        gpio->_port->ODR &= ~gpio->pin;
}

static inline void gpio_reset(struct tp_gpio * gpio)
{
    if (gpio->_port == NULL) return;
    
    if (gpio->dir == GPIO_DIR_INPUT) return;
    if (!gpio->invert)
        gpio->_port->ODR &= ~gpio->pin;
    else
        gpio->_port->ODR |= gpio->pin;
}

#ifdef __cplusplus
}
#endif

#endif //__DEV_GPIO_H_