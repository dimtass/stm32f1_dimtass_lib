#include "dev_gpio.h"

void gpio_init(struct tp_gpio * gpio)
{
    switch (gpio->port) {
        case GPIO_PORT_A:
            gpio->_port = GPIOA;
	        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
            break;
        case GPIO_PORT_B:
            gpio->_port = GPIOB;
	        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
            break;
        case GPIO_PORT_C:
            gpio->_port = GPIOC;
	        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
            break;
        case GPIO_PORT_D:
            gpio->_port = GPIOD;
	        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
            break;
        case GPIO_PORT_E:
            gpio->_port = GPIOE;
	        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
            break;
        case GPIO_PORT_F:
            gpio->_port = GPIOF;
	        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
            break;
        default:
            gpio->_port = NULL;
            return;
    }
    
	gpio->_config.GPIO_Speed = GPIO_Speed_2MHz;
	gpio->_config.GPIO_Pin = gpio->pin;
    if (gpio->dir == GPIO_DIR_INPUT)
        gpio->_config.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    else
	    gpio->_config.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(gpio->_port, &gpio->_config);

    if (gpio->init_value)
        gpio_set(gpio);
    else
        gpio_reset(gpio);
    TRACE(("init gpio[%d]\n", gpio->port));
}

struct tp_gpio * gpio_init_param(enum en_port_index port, uint16_t pin, enum en_gpio_dir dir, uint8_t init_value, uint8_t invert)
{
    struct tp_gpio * gpio = malloc(sizeof(struct tp_gpio));
    gpio->port = port;
    gpio->pin = pin;
    gpio->dir = dir;
    gpio->init_value = init_value;
    gpio->invert = invert;
    gpio->_port = NULL;
    gpio_init(gpio);
    return gpio;
}

void gpio_destroy(struct tp_gpio * gpio)
{
    free(gpio);
}